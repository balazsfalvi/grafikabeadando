# Számítógépi Grafika beadandó- Balázsfalvi Gergő 2020

 **Feladatleírás:** A koncepció egy múltbéli sivatagi környezet létrehozása. A feladatban egy szimuláció látható, amelyben be lehet járni a teret, alapvető mozgásokat figyelhetünk meg, illetve különböző funkciókat használhatunk.

# Funkciók, 

- Tér körbejárható
- 3D objektumok betöltése, bject betöltő libraryval
- Forgatható nap objektum
- Fényerő növelés, csökkentés
- Köd megjelenítése
- Jármű kőrkörös mozgatása
- Tevegelés gombyomásra
- AWP fegyver felvétele, áthelyezhetősége


## Használati útmutató

- Mozgatás
	>W A S D billentyűk
- Kamera forgatás
	>Bal klikk + egér mozgatás
- Objektum (nap) forgatása
	>X billentyűvel ki/be kapcsolás
- Fényerő növelés/csökkentés
	> " + " , " - " billentyűk
- HELP menü
	>t billentyű
- Köd létrehozása
	>f billentyű lenyomása (3 fokozat)
- Tevegelés
	>k billentyű
- Fegyver, AWP felvétel, mozgatás
	>J billentyű nyomva tartása, majd elengedése a letenni kívánt helyen
- Kilépés
	>ESC billentyű
## Telepítés: 

A teljes repositoryt szükséges letölteni, meghagyva a struktúráltásot.
(ZIP esetén kitömörítés, egyébként git clone https://gitlab.com/balazsfalvi/grafikabeadando.git - parancs kiadásával)
#### ASSETS mappa az alábbi linken található:
https://drive.google.com/file/d/11zS-OKUgYWY-Mv0qT6V_pK7JS1LQx5US/view?usp=sharing
- A letöltést követően ki kell tömöríteni. Az assets mappa a Makefile mellé kerüljön. (a root directoryba).
Ez tartalmaz egy objects, illetve egy textures mappát
Végezetül a Make parancs kiadása után egy sivatag.exe futtatható állományban tekinthető meg a program.

![alt text](http://www.balazsfalvi.infora.hu/egyetem/tutorial.jpg)


